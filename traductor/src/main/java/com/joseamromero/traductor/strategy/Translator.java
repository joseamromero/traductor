package com.joseamromero.traductor.strategy;

import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;
import org.springframework.stereotype.Service;

import com.joseamromero.traductor.exception.TranslateException;
import com.joseamromero.traductor.repository.Traduccion;
import com.joseamromero.traductor.repository.TraduccionRepository;

@Service
public class Translator {

	@Autowired
	TraduccionRepository repository;

	public Translator(Traduccion traduccion) {
		super();

	}

	public Translator() {

	}

	public String traducir(String mensajeIn, TranslateStrategiesEnum tse, String IP) {

		String textoTraducido = tse.getStrategy().traducir(mensajeIn);

		saveTraduccionEntity(mensajeIn, textoTraducido, new Date(), tse.getCodigo(),IP);

		return textoTraducido;
	}

	private void saveTraduccionEntity(String mensajeIn, String mensajeOut, Date fecha, String codigo, String IP) {
		Traduccion traduccion = new Traduccion(mensajeIn, mensajeOut, fecha, codigo,IP);
		repository.save(traduccion);

	}

}
