package com.joseamromero.traductor.strategy;

public enum TranslateStrategiesEnum {
	
	 BITStoMORSE(new TranslateBitsToMorseStrategy(), "BTM"), 
	 MORSEtoHUMAN(new TranslateMorseToHumanStrategy(), "MTH"),
	 HUMANtoMORSE(new TranslateHumanToMorseStrategy(), "HTM"); 
	
	private TranslatorStrategy strategy;
	private String codigo;	
	
	
	private TranslateStrategiesEnum(TranslatorStrategy strategy, String description) {
	    this.strategy = strategy ;
	    this.codigo = description ;
	  }


	public TranslatorStrategy getStrategy() {
		return strategy;
	}


	public String getCodigo() {
		return codigo;
	}
	
	

}
