package com.joseamromero.traductor.strategy;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.joseamromero.traductor.exception.TranslateException;

public class TranslateBitsToMorseStrategy implements TranslatorStrategy {

	private Pattern velocidadNormal = Pattern.compile("(1{4,6})|(1{1,3})|(0{4,5})|(0{1,3})");
	private Matcher matcher;

	@Override
	public String traducir(String mensajeIn) {

		this.matcher = velocidadNormal.matcher(mensajeIn);

		StringBuilder retorno = new StringBuilder();

		if (!this.matcher.find()) {
			throw new TranslateException("No se pudo realizar la traduccion de la secuencia de bits");
		} else {

			while (matcher.find()) {
				if (matcher.group(1) != null) {
					retorno.append("-");
				} else if (matcher.group(2) != null) {
					retorno.append(".");
				} else if (matcher.group(3) != null) {
					retorno.append(" ");
				} else if (matcher.group(3) != null) {
					retorno.append("");
				}
			}

		}
		return retorno.toString();
	}

	



}
