package com.joseamromero.traductor.strategy;

import java.util.HashMap;

import com.joseamromero.traductor.exception.TranslateException;


public interface TranslatorStrategy {
	public String traducir(String mensajeIn) throws TranslateException;

	
}
