package com.joseamromero.traductor.strategy;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.joseamromero.traductor.exception.TranslateException;

public class TranslateMorseToHumanStrategy implements TranslatorStrategy {

	@Override
	public String traducir(String mensajeIn) {

		validateFormat(mensajeIn);
		List<String> palabras = Arrays.asList(mensajeIn.split("  "));
		Map<String, String> diccionario = getDiccionario();
		StringBuffer traduccion = new StringBuffer();

		for (String palabra : palabras) {
			List<String> letras = Arrays.asList(palabra.split(" "));
			for (String letra : letras) {
				if (!letra.isEmpty()) {
					traduccion.append(diccionario.get(letra));
				}
			}
			traduccion.append(" ");
		}
		traduccion.deleteCharAt(traduccion.length() - 1);
		return traduccion.toString();
	}

	private void validateFormat(String mensajeIn)  {
		Pattern pattern = Pattern.compile("(.)|(-)|( )");
		Matcher matcher = pattern.matcher(mensajeIn);
		if (!matcher.find()) {
			throw new TranslateException("Solo se deben usar puntos, guiones y espacios en blanco en este servicio");
		}
	}

	private Map<String, String> getDiccionario() {

		Map<String, String> diccionario = new HashMap<String, String>() {
			{
				put(".-", "A");
				put("-...", "B");
				put("-.-.", "C");
				put("-..", "D");
				put(".", "E");
				put("..-.", "F");
				put("--.", "G");
				put("....", "H");
				put("..", "I");
				put(".---", "J");
				put("-.-", "K");
				put(".-..", "L");
				put("--", "M");
				put("-.", "N");
				put("---", "O");
				put(".--.", "P");
				put("--.-", "Q");
				put(".-.", "R");
				put("...", "S");
				put("-", "T");
				put("..-", "U");
				put("...-", "V");
				put(".--", "W");
				put("-..-", "X");
				put("-.--", "Y");
				put("--..", "Z");
				put("-----", "0");
				put(".----", "1");
				put("..---", "2");
				put("...--", "3");
				put("....-", "4");
				put(".....", "5");
				put("-....", "6");
				put("--...", "7");
				put("---..", "8");
				put("----.", "9");
				put(".-.-.-", ".");
				put("--..--", ",");
			}
		};

		return diccionario;
	}

}
