package com.joseamromero.traductor.repository;

import java.util.List;

import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TraduccionRepository extends CrudRepository<Traduccion, Long> {

	List<Traduccion> findByip(String ip);
	
	
}
