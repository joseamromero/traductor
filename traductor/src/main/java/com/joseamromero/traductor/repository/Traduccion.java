package com.joseamromero.traductor.repository;

import java.util.Date;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;

import com.fasterxml.jackson.annotation.JsonFormat;

@Entity
public class Traduccion {

	@Id
	@GeneratedValue(strategy = GenerationType.IDENTITY)
	private Long id;
	private String mensajeIn;
	private String mensajeOut;
	@JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
	private Date fecha;
	private String codigo;
	private String ip;

	protected Traduccion() {
	}

	public Traduccion(String mensajeIn, String mensajeOut, Date fecha, String codigo, String ip) {
		super();	
		this.mensajeIn = mensajeIn;
		this.mensajeOut = mensajeOut;
		this.fecha = fecha;
		this.codigo = codigo;
		this.setIp(ip);
	}

	@Override
	public String toString() {
		return String.format("Customer[id=%d, mensajeIn='%s', mensajeOut='%s', , codigo = %s  ]", id, mensajeIn,
				mensajeOut, codigo);
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public String getMensajeIn() {
		return mensajeIn;
	}

	public void setMensajeIn(String mensajeIn) {
		this.mensajeIn = mensajeIn;
	}

	public String getMensajeOut() {
		return mensajeOut;
	}

	public void setMensajeOut(String mensajeOut) {
		this.mensajeOut = mensajeOut;
	}

	public Date getFecha() {
		return fecha;
	}

	public void setFecha(Date fecha) {
		this.fecha = fecha;
	}

	public String getCodigo() {
		return codigo;
	}

	public void setCodigo(String codigo) {
		this.codigo = codigo;
	}

	public String getIp() {
		return ip;
	}

	public void setIp(String ip) {
		this.ip = ip;
	}

}
