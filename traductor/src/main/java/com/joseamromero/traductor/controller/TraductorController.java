package com.joseamromero.traductor.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.joseamromero.traductor.repository.Traduccion;
import com.joseamromero.traductor.repository.TraduccionRepository;
import com.joseamromero.traductor.strategy.TranslateStrategiesEnum;
import com.joseamromero.traductor.strategy.Translator;

@RestController
public class TraductorController {

	@Autowired
	private Translator translator;
	@Autowired 
	TraduccionRepository repository;

	@PostMapping(value = "translate/morseToHuman", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseMessage> decodeMorseToHuman(@RequestBody IncomingMessage message,
			HttpServletRequest request) {

		ResponseMessage text = new ResponseMessage(
				translator.traducir(message.getText(), TranslateStrategiesEnum.MORSEtoHUMAN, request.getRemoteAddr()));

		return new ResponseEntity<>(text, HttpStatus.OK);
	}

	@PostMapping(value = "translate/humanToMorse")
	public ResponseEntity<ResponseMessage> decodeHumanToMorse(@RequestBody IncomingMessage message,
			HttpServletRequest request) {

		ResponseMessage text = new ResponseMessage(
				translator.traducir(message.getText(), TranslateStrategiesEnum.HUMANtoMORSE, request.getRemoteAddr()));
		return new ResponseEntity<>(text, HttpStatus.OK);
	}

	@PostMapping(value = "translate/bitsToMorse", consumes = MediaType.APPLICATION_JSON_VALUE, produces = MediaType.APPLICATION_JSON_VALUE)
	public ResponseEntity<ResponseMessage> decodeBitToMorse(@RequestBody IncomingMessage message,
			HttpServletRequest request) {
		ResponseMessage text = new ResponseMessage(
				translator.traducir(message.getText(), TranslateStrategiesEnum.BITStoMORSE, request.getRemoteAddr()));
		return new ResponseEntity<>(text, HttpStatus.OK);
	}

	@GetMapping(value = "translate/findAll")
	public ResponseEntity<List<Traduccion>> findAll() {
		List<Traduccion> result = (List<Traduccion>) repository.findAll();
		return new ResponseEntity<>(result, HttpStatus.OK);

	}

	@GetMapping(value = "translate/findByip")
	public ResponseEntity<List<Traduccion>> findByIp(@RequestParam(required = true) String ip) {
		List<Traduccion> result = repository.findByip(ip);		
		return new ResponseEntity<>(result, HttpStatus.OK);

	}

}