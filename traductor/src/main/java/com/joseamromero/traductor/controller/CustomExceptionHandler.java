package com.joseamromero.traductor.controller;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import com.joseamromero.traductor.exception.TranslateException;


@RestControllerAdvice
public class CustomExceptionHandler {
	
	@ExceptionHandler(TranslateException.class)
	public ResponseEntity<ResponseMessage> handleException(final TranslateException e) {

		ResponseMessage text = new ResponseMessage(e.getMessage());

		return new ResponseEntity<>(text, HttpStatus.BAD_REQUEST);

	}

	@ExceptionHandler(Exception.class)
	public ResponseEntity<ResponseMessage> handleTranslateException(final Exception e) {

		ResponseMessage text = new ResponseMessage("Ocurrio un error desconocido");

		return new ResponseEntity<>(text, HttpStatus.INTERNAL_SERVER_ERROR);

	}
}