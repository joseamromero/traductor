package com.joseamromero.traductor.controller;

public class IncomingMessage {
	private String text;
	

	public IncomingMessage() {
	}

	public IncomingMessage(String text) {
		super();
		this.text = text;
	}

	public String getText() {
		return text;
	}

	public void setText(String text) {
		this.text = text;
	}

}
