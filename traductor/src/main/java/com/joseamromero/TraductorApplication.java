package com.joseamromero;


import java.util.Date;

import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.Bean;
import org.springframework.data.jpa.repository.config.EnableJpaRepositories;

import org.springframework.web.SpringServletContainerInitializer;
import org.springframework.web.servlet.config.annotation.EnableWebMvc;

import com.joseamromero.traductor.repository.Traduccion;
import com.joseamromero.traductor.repository.TraduccionRepository;

@EnableJpaRepositories(basePackages = "com.joseamromero.traductor.repository")
@SpringBootApplication
@EnableWebMvc
public class TraductorApplication extends SpringServletContainerInitializer {

	public static void main(String[] args) {
		SpringApplication.run(TraductorApplication.class, args);
	}

	@Bean
	public CommandLineRunner demo(TraduccionRepository repository) {
		return (args) -> {

			repository.save(new Traduccion("Para Traducir", "Traducido", new Date(), "ABC",""));
			repository.save(new Traduccion("Otra traduccion", "Respuesta", new Date(), "DEF",""));
			repository.save(new Traduccion("la ultima", "Es esta", new Date(), "HIJ",""));

	
		};
	}

}
