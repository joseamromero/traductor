package com.joseamromero.traductor;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;
import static org.springframework.test.web.servlet.setup.MockMvcBuilders.standaloneSetup;

import org.junit.Before;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.joseamromero.traductor.controller.IncomingMessage;
import com.joseamromero.traductor.controller.TraductorController;

@SpringBootTest
@RunWith(SpringRunner.class)
@AutoConfigureMockMvc
public class TraductorControllerTest {
	
	@Autowired
	private MockMvc mvc;
	@Autowired
	private TraductorController traductorController;

	@Before
	public void setUp() {
		mvc = standaloneSetup(traductorController).build();
	}

	@Test
	public void testDecodeMorseToHuman() throws Exception {

		String inputJson = this.mapToJson(new IncomingMessage(".... --- .-.. .-  -- . .-.. .."));
		mvc.perform(post("/translate/morseToHuman").accept(MediaType.APPLICATION_JSON).content(inputJson)
				.contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andExpect(status().isOk())
				.andExpect(content().json("{'response':'HOLA MELI'}"));
	}

	@Test
	public void testDecodeHumanToMorse() throws Exception {

		String inputJson = this.mapToJson(new IncomingMessage("HOLA MELI"));
		mvc.perform(post("/translate/humanToMorse").accept(MediaType.APPLICATION_JSON).content(inputJson)
				.contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8")).andExpect(status().isOk())
				.andExpect(content().json("{'response':'.... --- .-.. .-  -- . .-.. ..'}"));
	}

	@Test
	public void testDecodeBitsToMorse() throws Exception {

		String inputJson = this.mapToJson(new IncomingMessage(
				"000000001101101100111000001111110001111110011111100000001110111111110111000000001100011111100000111111001111110000000110000110111111110111000000011011100000000000"));
		mvc.perform(post("/translate/bitsToMorse").accept(MediaType.APPLICATION_JSON).content(inputJson)
				.contentType(MediaType.APPLICATION_JSON).characterEncoding("utf-8"))
				.andExpect(content().json("{'response':'.... --- .-.. .- -- . .-.. ..  '}"));
	}

	@Test
	public void testfindAll() throws Exception {

		mvc.perform(get("/translate/findAll")).andExpect(status().isOk());
	}

	private String mapToJson(Object obj) throws JsonProcessingException {
		ObjectMapper objectMapper = new ObjectMapper();
		return objectMapper.writeValueAsString(obj);
	}

}