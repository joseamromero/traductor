# Traductor Morse

Este traductor de codigo morse lo puedes consumir utilizando un request como el que te dejo de ejemplo:
Prueba a realizar una peticion: 
 
 - Morse a Humano:
```sh
curl -i -X POST -H "Content-Type:application/json" -d "{\"text\": \".... --- .-.. .-  -- . .-.. ..\"}" https://20200910t063306-dot-traductor-288923.rj.r.appspot.com/translate/morseToHuman

```

- Humano a Morse:
```sh
curl -i -X POST -H "Content-Type:application/json" -d "{\"text\": \"HOLA MELI\"}" https://20200910t063306-dot-traductor-288923.rj.r.appspot.com/translate/humanToMorse

```
- Bits a morse:
```sh
curl -i -X POST -H "Content-Type:application/json" -d "{\"text\": \"000000001101101100111000001111110001111110011111100000001110111111110111000000001100011111100000111111001111110000000110000110111111110111000000011011100000000000\"}" https://20200910t063306-dot-traductor-288923.rj.r.appspot.com/translate/bitsToMorse
```